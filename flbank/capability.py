"""Operations for interacting with objects via cryptocaps.
"""

from base64 import b64decode, b64encode
from hmac import compare_digest
from hashlib import sha256 as sha
from os import urandom
import struct


TOKEN_LENGTH = 32 # bytes
ID_FORMAT = ">q"


def unpack_token(key):
    try:
        raw_token = b64decode(key)
    except TypeError:
        return None, None
    object_id, = struct.unpack(ID_FORMAT, raw_token[-8:])
    auth_token = raw_token[:-8]
    return object_id, auth_token


def check_token(key_hash, token, salt):
    assert isinstance(token, bytes), token
    digest = sha(token + salt).hexdigest()
    return compare_digest(key_hash, digest)


def safe_get(model, token):
    """Fetch an instance of model using the capability token.
    """
    object_id, auth_token = unpack_token(token)
    if object_id is None:
        return None
    result = model.query.filter_by(id=object_id).first()
    if not result:
        return None
    if not check_token(result.digest, auth_token,
                       result.salt):
        return None
    return result


def get_well_known_token(instance, base_token):
    if not check_token(instance.digest, base_token,
                       instance.salt):
        return None
    result = base_token + struct.pack(ID_FORMAT, instance.id)
    return b64encode(result)


def provide(instance, token=None):
    """Generate a capability and add its public key to the instance.

    The instance must be present in the database first.
    """
    if token is None:
        token = urandom(TOKEN_LENGTH)
    salt = urandom(TOKEN_LENGTH)
    digest = sha(token + salt).hexdigest()
    instance.digest = digest
    instance.salt = salt
    return token + struct.pack(ID_FORMAT, instance.id)
