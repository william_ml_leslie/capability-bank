import enum
import hashlib
from broadway_sqlalchemy import extension as db

SIZE_NAME = 63
SIZE_EMAIL = 120
SIZE_DIGEST = hashlib.sha256().digest_size * 2
SIZE_SALT = 30
SIZE_CAPABILITY = 250
SIZE_ACCOUNTNO = 10


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(
        db.Unicode(SIZE_NAME), unique=True, nullable=False,
    )
    email = db.Column(db.Unicode(SIZE_EMAIL), unique=True, nullable=False)
    salt = db.Column(db.String(SIZE_SALT))
    digest = db.Column(db.String(SIZE_DIGEST))
    accountrefs = db.relationship("AccountRef", backref="user",
                                  lazy="dynamic")

    def __repr__(self):
        return "User(username=%r)" % (self.username,)


class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(SIZE_NAME))
    bsb = db.Column(db.Unicode(7))
    number = db.Column(db.Unicode(SIZE_ACCOUNTNO))
    amount = db.Column(db.Integer)
    accountrefs = db.relationship(
        "AccountRef", backref="account", lazy="dynamic"
    )
    accepted_transactions = db.relationship(
        "TransactionResponse", backref="account", lazy="dynamic"
    )
    requested_transactions = db.relationship(
        "TransactionRequest", backref="requesting_account", lazy="dynamic"
    )
    transactions = db.relationship(
        "Transaction", backref="account", lazy="dynamic"
    )

    def __repr__(self):
        return "Account(name=%r)" % (self.name,)


class AccountRef(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    salt = db.Column(db.String(SIZE_SALT))
    digest = db.Column(db.String(SIZE_DIGEST))
    petname = db.Column(db.Unicode(SIZE_NAME))
    primary = db.Column(db.Boolean, nullable=False)

    user_id = db.Column(
        db.Integer,
        db.ForeignKey(User.id),
        nullable=False,
    )
    account_id = db.Column(
        db.Integer,
        db.ForeignKey(Account.id),
        nullable=False,
    )


class TransactionBasis(enum.Enum):
    ONCE = "Once"
    BILL = "Bill"
    BOND = "Bond"
    ONGOING = "Ongoing"


class TransactionScale(enum.Enum):
    DAILY = "Daily"
    WEEKLY = "Weekly"
    FORTNIGHTLY = "Fortnightly"
    MONTHLY = "Monthly"
    BIMONTHLY = "Bi-monthly"
    YEARLY = "Yearly"


class TransactionResponse(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    salt = db.Column(db.String(SIZE_SALT))
    digest = db.Column(db.String(SIZE_DIGEST))
    account_id = db.Column(
        db.Integer,
        db.ForeignKey(Account.id),
        nullable=False,
    )
    amount = db.Column(db.Integer, nullable=False)
    basis = db.Column(db.Enum(TransactionBasis), nullable=False)
    limit_amount = db.Column(db.Integer)
    limit_scale = db.Column(db.Integer)
    send_to = db.Column(db.Unicode(SIZE_CAPABILITY))
    accepted = db.Column(db.Boolean, default=False, nullable=False)

    def __repr__(self):
        return "TransactionResponse(amount=%r, send_to=%r, basis=%r)" % (
            self.amount, self.send_to, self.basis
        )


class TransactionRequest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    salt = db.Column(db.String(SIZE_SALT))
    digest = db.Column(db.String(SIZE_DIGEST))
    requesting_account_id = db.Column(
        db.Integer,
        db.ForeignKey(Account.id),
        nullable=False,
    )
    amount = db.Column(db.Integer, nullable=False)
    basis = db.Column(db.Enum(TransactionBasis), nullable=False)
    limit_amount = db.Column(db.Integer)
    limit_scale = db.Column(db.Integer)
    request_from = db.Column(db.Unicode(SIZE_CAPABILITY), nullable=True)

    def __repr__(self):
        return "TransactionRequest(amount=%r, request_from=%r, basis=%r)" % (
            self.amount, self.request_from, self.basis,
        )


class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(
        db.Integer,
        db.ForeignKey(Account.id),
        nullable=False,
    )
    amount = db.Column(db.Integer, nullable=False)
    reconciled = db.Column(db.Boolean, default=False, nullable=False)

    def __repr__(self):
        return "Transaction(amount=%s, reconciled=%r)" % (
            self.amount, self.reconciled,
        )
