import broadway

factory = broadway.factory()

factory.add_extensions(
    'broadway_sqlalchemy',
    'broadway.whitenoise',
)

factory.add_blueprints(
    'flbank.blueprints.rest:rest',
)

