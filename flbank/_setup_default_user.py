from base64 import b64encode
from flbank.schema import db, User
from flbank.capability import provide


def create_sample_user(name="user", email="user@example.com"):
    user = User(username=name, email=email)
    db.session.add(user)
    db.session.commit()
    token = provide(user)
    db.session.commit()
    return b64encode(token)
