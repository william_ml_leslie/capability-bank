"""Services for interacting with accounts.

We currently have APIs defined for listing and creating accounts,
although many implementations are missing.  The aim is to get a few
actually rendering data on the frontend, and then iterate.

Notice these are (deliberately) not RESTful; even while we are
settling on the preferred pattern here we should remember to change
the route and module name.

Also note that cross_origin is fine, as any attacker that can name a
valid resource must be authorised.  On the other hand, our
capabilities are not very fine grained, so if you wanted to hand out
an account to others you'd need to create a ref.

"""

import random
from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from flbank.capability import (safe_get, get_well_known_token, provide,
                               unpack_token)
from flbank.schema import db, User, AccountRef, Account

def json_error(text, status=400):
    return make_response(jsonify({"status": "failure", "error": text}),
                         status)

rest = Blueprint('rest', __name__, url_prefix='/api/rest/v1.0')


@rest.route('/accounts', methods=['POST'])
@cross_origin()
def accounts():
    try:
        data = request.get_json()
        user_cap = data["user"]
    except (ValueError, KeyError):
        return json_error("Invalid request body")
    print("cap:", user_cap)
    user = safe_get(User, user_cap)
    _, user_cap_base = unpack_token(user_cap)
    if not user:
        return json_error("User not found", 404)
    accounts = []
    for ref in user.accountrefs.filter_by(primary=True):
        account = ref.account
        accounts.append({
            "petname": ref.petname,
            "name": account.name,
            "bsb": account.bsb,
            "number": account.number,
            "amount": account.amount,
            "url": get_well_known_token(ref, user_cap_base).decode('ascii'),
        })
    return {"status": "success", "accounts": accounts}


@rest.route('/accounts-new', methods=['POST'])
@cross_origin()
def accounts_new():
    try:
        data = request.get_json()
        user_cap = data["user"]
        name = data["account-name"]
    except (ValueError, KeyError):
        return json_error("Invalid request body")
    user = safe_get(User, user_cap)
    if not user:
        return json_error("User not found", 404)

    account_number = u"".join(random.choice("0123456789") for _ in range(8))
    account = Account(
        name=name, bsb="333-333", number=account_number, amount=0
    )
    db.session.add(account)
    db.session.commit()
    account_ref = AccountRef(
        primary=True, user_id=user.id, account_id=account.id
    )
    db.session.add(account_ref)
    db.session.commit()
    _, user_cap_base = unpack_token(user_cap)
    provide(account_ref, user_cap_base)
    db.session.commit()
    url = get_well_known_token(account_ref, user_cap_base).decode('ascii')
    return {
        "status": "success",
        "url": url,
    }


@rest.route('/transaction-requests', methods=['POST'])
@cross_origin()
def transaction_requests():
    return {"status": "not-implemented"}


@rest.route('/transactions', methods=['POST'])
@cross_origin()
def list_transactions():
    return {"status": "not-implemented"}


@rest.route('/payment-request', methods=['POST'])
@cross_origin()
def payment_request():
    return {"status": "not-implemented"}


@rest.route('/payment-show', methods=['GET', 'POST'])
@cross_origin()
def payment_show():
    return {"status": "not-implemented"}


@rest.route('/payment-fulfil', methods=['POST'])
@cross_origin()
def payment_fulfil():
    return {"status": "not-implemented"}

