Capability Bank - Flask Backend
===============================

Install the dependencies for the backend with::

  export VENV=.venv
  virtualenv -p python3 $VENV
  source $VENV/bin/activate
  pip install -r requirements.txt

Run the flask server with::

  source $VENV/bin/activate
  export FLASK_APP=wsgi.py
  export SERVER_NAME=127.0.0.1:5000
  export SECRET_KEY="$(python2 -c 'import os; print(os.urandom(32).decode("latin1").encode("utf-8"))')"
  export DEBUG=True
  export SQLALCHEMY_DATABASE_URI=sqlite:///sample.db
  flask run

Install the dependencies for the frontend with::

  cd bank-frontend
  nvm use
  yarn install

Run the yarn dev server with::

  yarn start

To be fair, before you're able to do anything on the site, you'll need
to create a user.  To do so::

  flask shell
  from flbank._setup_default_user import create_sample_user
  create_sample_user()

The returned token is your login token.  You can use this to access
the page at::

  http://localhost:3000/accounts/list#!your-token-here
