Overview
--------

The capability-bank client enables you to:

* Create a user, and create accounts belonging to them

* From an account, create a ref (allowing the holder to either manage
  the account, read transactions, or read the available balance)

* Create an invoice (payment request) and share it.  This produces an
  url (and a QR code) that can be shared with someone for payment.

* View an invoice and fulfil it

* Add the account refs of others to your own account list.

It's supposed to be kind of pretty, but it needs work on that front;
not only fonts and logos but the tables and forms aren't actually
responsive.

Users and Accounts
------------------

The sample interface allows anyone to create a user by specifying
their username.  We don't ask for an email here, because it's unused
on the client.

In the server backed version, there is probably some external process
for creating users.  It's not clear if that should be an admin thing
or generally by application.

You can also create all the accounts you want.  In the sample, they
will magically contain a little money.

Refs
----

It would be nice to have better terminology for these, but they are
basically keys to your account that you can hand out.  You can create
refs that convey the ability to use your account as you would -
including using transferring money out.  You can also create refs that
only allow the holder to see your transactions or to see the current
balance.  This makes it easy to build financial dashboards to track
your spending without creating a security hole.

Invoices
--------

You can create invoices from the ``Invoices`` tab.  For a regular
invoice, just set the amount and a name for the line as it should
appear in the client account.

You can also create special invoices for the many other things you can
pay for with a credit card, such as regular bills.  On these, set a
time scale for regular bills and a maximum billable amount (above
which, renegotiation must occur).  It's reassuring for a client to
know that there's a maximum amount that you can take from their
account without their approval, and we're not sure why this hasn't
always been the standard.
