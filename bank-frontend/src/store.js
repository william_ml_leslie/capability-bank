import { createStore, combineReducers, applyMiddleware } from 'redux'
import { reducer as formReducer } from 'redux-form'
import thunkMiddleware from 'redux-thunk';
import * as Actions from './app/actions';


function currentUser(state = {}, action) {
    if (action.type === Actions.CREATE_USER) {
        return { name: action.name };
    } else if (action.type === Actions.LOGIN_USER) {
        return { name: action.name };
    }
    return state;
}


function lenseNew(collection, value) {
    return { ...collection, [value.id]: value };
}


function lenseKey(collection, id, mutator) {
    return {
        ...collection,
        [id]: mutator(collection[id])
    };
}


function account(state = {}, action) {
    if (action.type === Actions.CREATE_USER) {
        // let's give them an account to start with.
        return lenseNew(state, {
            id: -1,
            name: "Good Sport Account",
            accountType: "MERCHANT",
            amount: 20000,
            bsb: "123-123",
            number: "1234-1234",
            transactions: [
                {amount: 20000, label: "Testing Gift", timestamp: "totally"},
            ],
        });
    } else if (action.type === Actions.CREATE_ACCOUNT) {
        const { name, accountType, id } = action;
        const amount = Math.floor(Math.random() * 50000);
        return lenseNew(state, {
            id,
            name,
            accountType,
            amount,
            bsb: "123-123",
            number: `1234-${id}`,
            transactions: [
                {amount, label: "Testing Gift", timestamp: "totally"},
            ],
        });
    } else if (action.type === Actions.CREATE_TRANSACTION) {
        const { account: accountId, amount, to, label } = action;
        return lenseKey(state, accountId, account => (
            account.amount < amount ? account : lenseKey(
                account, "transactions", transactions => (
                    [...transactions, ({ amount, to, label })]
                )
            )
        ));
    } else if (action.type === Actions.LOGIN_USER) {
        return action.accounts.reduce((st, account) => {
            const { id, name, accountType, amount, bsb, number } = account;
            return lenseNew(st, {
                id,
                name,
                accountType,
                amount,
                bsb,
                number,
                transactions: [
                    {amount, label: "Initial", timestamp: "totally"},
                ],
            });
        }, state);
    }
    return state;
}


function paymentRequest(state = {}, action) {
    if (action.type === Actions.CREATE_PAYMENT_REQUEST) {
        const { name, amount, basis, limitAmount, limitScale, id } = action;
        return lenseNew(state, {
            id, name, amount, basis, limitAmount, limitScale,
        });
    } else if (action.type === Actions.ACCEPT_PAYMENT_REQUEST) {
        const { name, request } = action;
        return lenseKey(state, request, match => ({
            acceptedBy: name,
        }))
    }
    return state;
}


function lensePetnames(keys, id, mutator) {
    return lenseKey(keys, id, petname => ({
        ...petname,
        accounts: mutator(petname.accounts || [])
    }))
}


function keys(state = {}, action) {
    if (action.type === Actions.CREATE_PETNAME) {
        const { id, name, rights, account } = action;
        const accounts = account === null ? [] : [account];
        return lenseNew(state, { id, name, rights, accounts });
    } else if (action.type === Actions.DELETE_PETNAME) {
        const { id } = action;
        const newstate = Object.assign({}, state);
        delete newstate[id];
        return newstate;
    } else if (action.type === Actions.ALTER_PETNAME) {
        const { id, name } = action;
        return lenseKey(state, id, petname => ({ ...petname, name }));
    } else if (action.type === Actions.PETNAME_ADD_ACCOUNT) {
        const { id, account, perms } = action;
        return lensePetnames(state, id, accounts => (
            [...accounts, { account, perms }]
        ));
    } else if (action.type === Actions.PETNAME_DELETE_ACCOUNT) {
        const { id, account } = action;
        return lensePetnames(state, id, accounts => (
            accounts.filter(ref => ref.acount !== account)
        ));
    } else if (action.type === Actions.PETNAME_EDIT_ACCOUNT) {
        const { id, account, perms } = action;
        return lensePetnames(state, id, accounts => (
            accounts.map(ref => (
                ref.account !== account ? ref : ({ ...ref, perms})
            ))
        ));
    } else if (action.type === Actions.LOGIN_USER) {
        return Object.values(action.keys).reduce((st, key) => {
            return lenseNew(st, key);
        }, state);
    }
    return state;
}


const rootReducer = combineReducers({
    currentUser,
    account,
    paymentRequest,
    keys,
    form: formReducer
})

export const store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware)
)
