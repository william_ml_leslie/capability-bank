import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route, useHistory, useLocation } from 'react-router-dom';
import AccountRoutes from './accounts/routes';
import TransactionRoutes from './transactions/transaction-routes';
import UserRoutes from './user/routes';
import NewUser from './user/new-user';
import PaymentRequestRoutes from './payment-request/routes';
import PetnameRoutes from './petnames/routes';


function PageMissing() {
    return (
        <div>Not Found!</div>
    );
}

function AppRoutes({ user }) {
    const history = useHistory();
    const { hash, pathname } = useLocation();
    if (!user || !user.name) {
        if (pathname !== "/user/new" &&
            ((pathname !== "/user" && pathname !== "/payments/show")
             || !hash)) {
            history.replace("/user/new")
            return (<NewUser />);
        }
    }
    return (
        <Switch>
            <Route path="/accounts">
                <AccountRoutes />
            </Route>
            <Route path="/transactions">
                <TransactionRoutes />
            </Route>
            <Route path="/payments">
                <PaymentRequestRoutes />
            </Route>
            <Route path="/petnames">
                <PetnameRoutes />
            </Route>
            <Route path="/user">
                <UserRoutes />
            </Route>
            <Route path="/">
                User details
            </Route>
            <Route>
                <PageMissing />
            </Route>
        </Switch>
    );
}

export default connect(state => ({ user: state.currentUser }))(AppRoutes);
