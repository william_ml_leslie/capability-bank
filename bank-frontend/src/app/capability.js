/*
 * Operations for interacting with objects via cryptocaps.
 *
 * This is the client-side mock for capabilitybank.capability.
 *
 * If there was a server side, we'd generate a salted key pair that we
 * can use to securely talk about the object.  However, we've got no
 * server that we can use as a rendezvous!  So instead, we'll fake
 * these capabilities by embedding details that need to be replicated
 * in the other system.
 */


function encode(obj) {
    const encoder = new TextEncoder("utf-8");
    const utf8 = encoder.encode(JSON.stringify(obj));
    const result = [];
    // mapping over byte arrays generates another byte array; so we
    // need a for loop here.
    utf8.forEach(codePoint => {
        result.push(String.fromCharCode(codePoint));
    });
    return btoa(result.join(""));
}

export function decode(token) {
    const decoder = new TextDecoder("utf-8");
    const b64 = atob(token);
    const utf8 = new Uint8Array(b64.length);
    for (var i = 0; i < b64.length; i++) {
        utf8[i] = b64.charCodeAt(i);
    }
    return JSON.parse(decoder.decode(utf8));
}

export function buildUserCap(name, accounts, keys) {
    // strip transactions
    const simpleAccounts = Object.values(accounts).map(({
        id, name, accountType, amount, bsb, number
    }) => ({
        id, name, accountType, amount, bsb, number
    }));
    return encode({ name, accounts: simpleAccounts, keys });
}

export function buildPaymentRequestCap(paymentRequest) {
    const { id, amount, basis, limitAmount, limitScale, userName } =
          paymentRequest;
    const obj = { id, amount, basis, userName };
    if (["ONGOING", "BILL"].includes(basis)) {
        obj.limitAmount = limitAmount;
        obj.limitScale = limitScale;
    }
    return encode(obj);
}

export function buildPetnameCap(key) {
    const { id, name, rights, accounts } = key;
    const cleanAccounts = accounts.map(({ id, name, amount }) => (
        { id, name, amount }
    ));
    return encode({ id, name, rights, accounts: cleanAccounts });
}
