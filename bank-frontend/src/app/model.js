export const BASIS = {
    ONCE: "Once - a single payment",
    BILL: "Bill - a regular, varying payment",
    BOND: "Bond - transactional borrow",
    ONGOING: "Ongoing - a regular, fixed payment",
};

export const BASIS_OPTIONS = [
    "ONCE", "BILL", "BOND", "ONGOING"
].map(value => ({value, label: BASIS[value]}));


export const SCALE = {
    DAILY: "Daily",
    WEEKLY: "Weekly",
    FORTNIGHTLY: "Fortnightly",
    MONTHLY: "Monthly",
    BIMONTHLY: "Bi-monthly",
    YEARLY: "Yearly",
};

export const SCALE_OPTIONS = [
    "DAILY", "WEEKLY", "FORTNIGHTLY", "MONTHLY", "BIMONTHLY", "YEARLY"
].map(value => ({value, label: SCALE[value]}));
