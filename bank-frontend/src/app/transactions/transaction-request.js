import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { Input, Select } from '../components/form';


const formSelector = formValueSelector('new-transaction-request');


const BASIS = [
    {value: "ONCE", label: "Once - a single payment"},
    {value: "BILL", label: "Bill - a regular, varying payment"},
    {value: "BOND", label: "Bond - transactional borrow"},
    {value: "ONGOING", label: "Ongoing - a regular, fixed payment"},
];

const SCALE = [
    {value: "DAILY", label: "Daily"},
    {value: "WEEKLY", label: "Weekly"},
    {value: "FORTNIGHTLY", label: "Fortnightly"},
    {value: "MONTHLY", label: "Monthly"},
    {value: "BIMONTHLY", label: "Bi-monthly"},
    {value: "YEARLY", label: "Yearly"},
];


function NewRequestForm({ handleSubmit, basis }) {
    const once = ["ONCE", "BOND"].includes(basis);

    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <div className="col">
                    <Input
                        name="amount"
                        label="Amount"
                        help="The amount of the invoice."
                        inputType="number"
                        prepend="$"
                    />
                </div>
                <div className="col">
                    <Select
                        name="basis"
                        label="Transaction Type"
                        help="The type of the transaction."
                        options={BASIS}
                    />
                </div>
                <div className="col">
                    <Input
                        name="limitAmount"
                        label="Limit Amount"
                        help="The maximum amount chargeable."
                        inputType="number"
                        prepend="$"
                        disabled={once}
                    />
                </div>
                <div className="col">
                    <Select
                        name="limitScale"
                        label="Limit Scale"
                        help="The time period over which the limit applies."
                        options={SCALE}
                        disabled={once}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </div>
        </form>
    );
}


function mapStateToProps(state) {
    return {
        basis: formSelector(state, 'basis'),
    };
}


const ConnectedRequestForm = reduxForm({
  form: 'new-transaction-request'
})(connect(mapStateToProps)(NewRequestForm));


export default function NewRequest() {
    const initial = {
        basis: "ONCE",
        amount: 200,
    };
    return (
        <div className="container">
            <ConnectedRequestForm initialValues={initial} />
        </div>
    );
}
