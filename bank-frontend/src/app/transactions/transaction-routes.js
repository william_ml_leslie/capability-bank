import React from 'react';
import { useRouteMatch, Switch, Route } from 'react-router-dom';
import NewRequest from './transaction-request';


export default function TransactionRoutes() {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/new`}>
                <p className="lede">New Transaction Request</p>
                <NewRequest />
            </Route>
            <Route path={`${path}/list`}>
                List Transacton Requests
            </Route>
            <Route path={`${path}/show`}>
                Show Request Status
            </Route>
        </Switch>
    );
}
