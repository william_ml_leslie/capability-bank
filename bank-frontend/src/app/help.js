import React from 'react';
import { Route, Switch } from 'react-router-dom';


const ALERT = "alert alert-info mt-3 small";


function AccountList() {
    return (
        <div className={ALERT}>
            <h3>Accounts</h3>
            <p>
              Here are the accounts you have access to.  If
              you like, feel free to create a new account,
              as it makes building and using shared keys easier.
            </p>
        </div>
    );
}

function AccountShow() {
    return (
        <div className={ALERT}>
            <h3>Account</h3>
            <p>
              Here are your account details, including your
              transaction log if you have the authority to
              read it, and any shared keys you have added the
              account to.
            </p>
        </div>
    );
}

function AccountNew() {
    return (
        <div className={ALERT}>
            <h3>Create a new Account</h3>
            <p>
              Some parts of the site are more fun to test
              with additional accounts, so create a new one
              here.
            </p>
        </div>
    );
}

function InvoiceDetail() {
    return (
        <div className={ALERT}>
            <h3>Invoice</h3>
            <p>
              This is the detail of the invoice.  This URL is
              used by the bank of the patron to give evidence
              of successful payment.
            </p>
            <p>
              To pay this invoice, log-in to your banking website
              and use the Pay Invoice feature.
            </p>
        </div>
    );
}

function InvoiceMerchantShow() {
    return (
        <div className={ALERT}>
            <h3>Invoice</h3>
            <p>
              The customer can use the generated URL to complete
              payment.  They may use the QR code from within the
              app, or they could paste the given URL into their
              banking portal.
            </p>
            <p>
              By going through their bank, the customer can be
              confident that exactly the amount described on
              the invoice will be charged.  You cannot adjust the
              transaction amount without their permission, nor can
              you double charge them.
            </p>
            <p>
              Their bank will notify you immediately on payment, so
              you have verification that the payment was successful.
            </p>
        </div>
    );
}

function InvoicePatronShow() {
    return (
        <div className={ALERT}>
            <h3>Pay An Invoice</h3>
            <p>
              You can view the details of an invoice you have been
              given and pay it with this page.  Since this page
              is on your own banks website, you can be confident
              that what you are paying is exactly what is shown here.
            </p>
        </div>
    );
}

function InvoiceNew() {
    return (
        <div className={ALERT}>
            <h3>Build an invoice</h3>
            <p>
              Create an invoice - a request for payment.  Once
              you have selected the type and details of the
              invoice, you can hand it to the customer for payment.
            </p>
        </div>
    );
}

function KeyList() {
    return (
        <div className={ALERT}>
            <h3>Build a Shared Key</h3>
            <p>
              Create a key that you can share with third-party
              financial services.  You can restrict the key and
              revoke it at any time.
            </p>
        </div>
    );
}

function KeyShow() {
    return (
        <div className={ALERT}>
            <h3>Shared Key</h3>
            <p>
              This is the details of your Shared Key.
            </p>
        </div>
    );
}

function UserDisplay() {
    return (
        <div className={ALERT}>
            <h3>Your login details.</h3>
            <p>
              You can bookmark this page so that you can
              automatically log back in.
            </p>
            <p>
              In a real banking website, we would probably ask
              you for a second factor as well, but since this
              is all made-up data we will leave it there.
              For most purposes, a link of the sort
              you are bookmarking is far more secure than
              password + SMS.
            </p>
            <p>
              Please use the Invoice section to see a more
              secure alternative to credit cards, and the
              Shared Keys section to manage access to your
              accounts by third-party financial dashboards and
              other tools.
            </p>
        </div>
    );
}

function UserNew() {
    return (
        <div className={ALERT}>
            <h3>New User</h3>
            <p>
              Create a user to experiment with the site.
            </p>
        </div>
    );
}

function NothingYet() {
    return (
        <div className={ALERT}>
            I should write some help text for this section.
        </div>
    );
}

export default function Help() {
    return (
        <Switch>
            <Route path="/accounts/new" component={AccountNew} />
            <Route path="/accounts/list" component={AccountList} />
            <Route path="/accounts/show" component={AccountShow} />
            <Route path="/accounts" component={AccountList} />
            <Route path="/transactions/new" component={NothingYet} />
            <Route path="/transactions/show" component={NothingYet} />
            <Route path="/payments/new" component={InvoiceNew} />
            <Route path="/payments/list" component={NothingYet} />
            <Route path="/payments/pay" component={InvoicePatronShow} />
            <Route path="/payments/show" component={InvoiceDetail} />
            <Route
                path="/payments/:paymentId"
                component={InvoiceDetail}
            />
            <Route path="/payments" component={InvoiceNew} />
            <Route path="/petnames/:petnameId" component={KeyShow} />
            <Route path="/petnames" component={KeyList} />
            <Route path="/user/new" component={UserNew} />
            <Route path="/user" component={UserDisplay} />
            <Route component={NothingYet} />
        </Switch>
    );
}
