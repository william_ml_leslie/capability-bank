import React from 'react';
import { Link } from 'react-router-dom';


export default function AccountList({ accounts }) {
    return (
        <>
         {accounts.map(account => (
             <div
                 key={account.id}
                 className="row align-items-center mb-1 bg-light"
             >
                 <div className="col">
                     <Link to={`accounts/show/${account.id}`}>
                         {account.name}
                     </Link>
                 </div>
                 <div className="col">
                     ${(account.amount / 100).toFixed(2)}
                 </div>
                 <div className="col">
                     <small>BSB: {account.bsb}</small><br />
                     <small>AN: {account.number}</small>
                 </div>
             </div>
         ))}
        </>
    );
}
