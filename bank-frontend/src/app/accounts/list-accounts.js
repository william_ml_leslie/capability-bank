import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AccountList from './account-list';
import Help from '../help';


function ListAccounts({ accounts, username }) {
    if (accounts) {
        return (
            <div className="container">
                <Help />
                <p>
                  Here are your accounts, {username}!
                </p>
                <AccountList accounts={accounts} />
                <br />
                <div
                    className="d-flex justify-content-between"
                    role="toolbar"
                    aria-label="Account Toolbar"
                >
                    <p>
                    </p>
                    <Link
                        to="/accounts/new"
                        className="btn btn-success"
                    >
                        New Account
                    </Link>
                </div>
            </div>
        );
    }

    return (
        <div>Failed to load your accounts.</div>
    );
}

export default connect(state => ({
    accounts: Object.values(state.account),
    username: state.currentUser.name
}))(ListAccounts);
