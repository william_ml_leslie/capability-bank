import React from 'react';
import { useRouteMatch, Switch, Route } from 'react-router-dom';
import NewAccount from './new-account';
import ListAccounts from './list-accounts';
import ShowAccount from './account-show';


export default function AccountRoutes() {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/new`}>
                <NewAccount />
            </Route>
            <Route path={`${path}/list`}>
                <ListAccounts />
            </Route>
            <Route path={`${path}/show/:id`}>
                <ShowAccount />
            </Route>
            <Route>
                <ListAccounts />
            </Route>
        </Switch>
    );
}
