import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { useHistory } from 'react-router';
import { createAccount } from '../actions';
import { Input, Select } from '../components/form';
import { ACCOUNT_TYPE_LABEL } from '../constants';
import Help from '../help';

const ACCOUNT_TYPES = [
    {value: "EVERYDAY", label: ACCOUNT_TYPE_LABEL.EVERYDAY},
    {value: "SAVINGS", label: ACCOUNT_TYPE_LABEL.SAVINGS },
    {value: "MERCHANT", label: ACCOUNT_TYPE_LABEL.MERCHANT },
];

const ACCOUNT_DESCRIPTIONS = {
    "EVERYDAY": "With 200 transactions FREE every week!",
    "SAVINGS": "Earn 4%pa every month with no more than one withdrawal",
    "MERCHANT": "Fixed-cost account with unlimited transactions!",
};


const formSelector = formValueSelector('new-account');


function NewAccountForm({ accountType, handleSubmit }) {
    return (
        <form onSubmit={handleSubmit}>
             <div className="row">
                <div className="col">
                   <Input
                        name="name"
                        label="Account Name"
                        help="A name you can use to identify your account."
                    />
                </div>
                <div className="col">
                    <Select
                        name="accountType"
                        label="Account Type"
                        help={ACCOUNT_DESCRIPTIONS[accountType]}
                        options={ACCOUNT_TYPES}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                    Apply!
                </button>
            </div>
        </form>
    );
}

const ConnectedAccountForm = reduxForm({
  form: 'new-account'
})(connect(state => ({
    accountType: formSelector(state, 'accountType')
}))(NewAccountForm));


function NewAccount(props) {
    const initial = {
        accountType: "EVERYDAY"
    };
    const history = useHistory();
    const onSubmit = data => props.createAccount(data).then(
        id => history.push(`/accounts/show/${id}`)
    );
    return (
        <div className="container">
            <Help />
            <ConnectedAccountForm initialValues={initial} onSubmit={onSubmit}/>
        </div>
    );
}

export default connect(null, { createAccount })(NewAccount);
