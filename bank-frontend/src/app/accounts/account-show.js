import React from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
//import { Link } from 'react-router-dom';
//import AccountList from './account-list';
import { ACCOUNT_TYPE_LABEL } from '../constants';
import Help from '../help';


function NewPetnameRow() {
    return (
        <div className="input-group">
            <input placeholder="Name" className="form-control" />
            <div className="input-group-append">
                <button className="btn btn-outline-success">
                    Add ref
                </button>
            </div>
        </div>
    );
}


function Rights({ rights }) {
    if (rights.includes("w")) {
        return "Can take money from this account";
    } else if (rights.includes("r")) {
        return "Can read transaction history";
    } else {
        return "Can read the current balance";
    }
}


function Petnames({ petnames }) {
    return (
        <ul className="list-group">
            <li className="list-group-item">
                Links to this account
            </li>
            {petnames.map(({ name, rights }) => (
                <li key={name} className="list-group-item">
                    <div className="d-flex justify-content-between">
                        <div>
                            {name} - <Rights rights={rights} />
                        </div>
                        <div>
                            <button className="btn btn-danger">
                                Delete
                            </button>
                        </div>
                    </div>
                    <br />
                    <input
                        value={"snthoanehuanoehu"}
                        className="form-control"
                    />
                </li>
            ))}
            <li className="list-group-item">
                <NewPetnameRow />
            </li>
        </ul>
    );
}

function petnameStateToProps(state, { accountId }) {
    const petnames = [];
    Object.values(state.keys).forEach(({ name, rights, id, accounts }) => {
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i].account === accountId ) {
                petnames.push({ id, name, rights });
                break;
            }
        }
    })
    return {
        petnames
    };
}

const ConnectedPetnames = connect(petnameStateToProps)(Petnames);


function ShowAccount({ account }) {
    if (!account) {
        return (
            <div className="alert-warning">Account not found.</div>
        );
    }
    const { id, name, accountType, amount, number } = account;
    return (
        <div className="row justify-content-around">
            <div className="col-6-sm">
                <ul className="list-group">
                    <li className="list-group-item">
                        Name: {name} ({ACCOUNT_TYPE_LABEL[accountType]})
                    </li>
                    <li className="list-group-item">
                        Account Number: {number}
                    </li>
                    <li className="list-group-item">
                        Balance Available: ${(amount / 100).toFixed(2)}
                    </li>
                </ul>
            </div>
            <div className="col-6-sm">
                <ConnectedPetnames accountId={id} />
            </div>
        </div>
    );
}


const ConnectedShowAccount = connect((state, { id }) => ({
    account: state.account[id],
}))(ShowAccount);


export default function ShowAccountFromRoute() {
    const { id } = useParams();
    return (
        <div className="container">
            <Help />
            <ConnectedShowAccount id={id} />
        </div>
    );
}
