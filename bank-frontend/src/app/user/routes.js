import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import NewUser from './new-user';
import DisplayUser from './display-user';


export default function AppRoutes() {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/login`}>
                <NewUser />
            </Route>
            <Route path={`${path}/new`}>
                <NewUser />
            </Route>
            <Route>
                <DisplayUser />
            </Route>
            <Route path={`${path}/logout`}>
                <div>No logout for you!</div>
            </Route>
        </Switch>
    );
}
