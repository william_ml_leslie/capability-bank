import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { useHistory } from 'react-router-dom';
import { Input } from '../components/form';
import { createUser } from '../actions';
import Help from '../help';


function NewAccountForm({ accountType, handleSubmit }) {
    return (
        <form onSubmit={handleSubmit}>
           <Input
                name="name"
                label="User Name"
                help={""}
            />
            <p className="text-right"><em>Yours, securely.</em></p>
            <p><small>
              Note that this is the client-side only version of
              the site, so we will not be storing anything.  You
              can be whoever you want to be for the sake of experiment.
            </small></p>
            <p><small>
              We will even give you a little totally not real money
              to experiment with.
            </small></p>
            <div className="d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                    Apply!
                </button>
            </div>
        </form>
    );
}

const ConnectedAccountForm = reduxForm({
  form: 'new-user'
})(NewAccountForm);


function NewUser(props) {
    const initial = {
        name: "Alice"
    };
    const history = useHistory();
    const onSubmit = data => (
        props.createUser({ ...data, amount: 20000 }).then(
            () => history.push("/user")
        )
    );
    return (
        <div className="container">
            <Help />
            <ConnectedAccountForm
                initialValues={initial}
                onSubmit={onSubmit}
            />
        </div>
    );
}

export default connect(null, { createUser })(NewUser)
