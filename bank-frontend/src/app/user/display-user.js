import React from 'react';
import { connect } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import { buildUserCap, decode } from '../capability';
import { loginUser } from '../actions';
import Help from '../help';


function UserDisplay({ name, login, account, keys }) {
    const history = useHistory();
    let { pathname, hash } = useLocation();
    if (!hash) {
        if (!name || "" === name) {
            history.push("/user/new");
        } else if (!hash) {
            hash = buildUserCap(name, account, keys);
            history.replace(`${pathname}#!${hash}`);
        }
    }
    React.useEffect(() => {
        if (hash && !name) {
            const user = decode(hash.slice(2));
            login(user);
        }
    }, [hash, name, login]);
    return (
        <div className="container">
            <Help />
            <div>
                <h2 className="h2">
                  Welcome to Capability Bank, {name}!
                </h2>
                <p>
                  Please bookmark this page so that you can easily
                  log back in.
                </p>
                <p>
                  This is a testing ground for using capability
                  concepts in a banking workflow.  Try clicking
                  things and see if you can figure out what is
                  going on.
                </p>
                <p>
                  Again, none of the data input to this app
                  leaves your computer except via your monitor
                  or screen reader.  This definitely makes it less
                  useful for actual banking, but I guess it makes
                  it more useful for evaluation.
                </p>
            </div>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        name: state.currentUser ? state.currentUser.name : null,
        keys: state.keys,
        account: state.account,
    };
}

export default connect(mapStateToProps, { login: loginUser })(UserDisplay);
