import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { useParams } from 'react-router-dom';
import { Select } from '../components/form';
import ReactQRCode from '../components/react-qrcode';
import { buildPetnameCap } from '../capability';
import { petnameAddAccount } from '../actions';
import Help from '../help';


const FORM_NAME = "petname-add-account";

const HOST = "http://localhost:3000";
const BASE = process.env.REACT_APP_BASENAME || "";

const PERMS = [
    {label: "Full Access", value: "F"},
    {label: "Read Transactions", value: "T"},
    {label: "Read Available Amount", value: "A"},
];

function AddAccountForm({ handleSubmit, accounts }) {
    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <div className="col">
                    <Select
                        name="account"
                        label="Account"
                        help="Select an account to add"
                        options={accounts}
                    />
                </div>
                <div className="col">
                    <Select
                        name="perms"
                        label="Permissions"
                        help="Choose a permission level"
                        options={PERMS}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                    Add
                </button>
            </div>
        </form>
    );
}

const ConnectedAddAccountForm =
      reduxForm({ form: FORM_NAME })(AddAccountForm);

function InitialisedAddAccountForm({ accounts, add }) {
    if (accounts.length === 0) {
        return <div>no other accounts</div>;
    }

    const initial = {
        account: accounts[0].value,
        perms: "A",
    };
    return (
        <ConnectedAddAccountForm
            enableReinitialize
            accounts={accounts}
            onSubmit={add}
            initialValues={initial}
        />
    );
}

function AccountList({ accounts }) {
    return (
        <div className="d-flex flex-column">
            {accounts.map(({ perms, name, amount }) => (
                <div className="row" key={name}>
                    {name} {amount} {perms} [Remove]
                </div>
            ))}
        </div>
    );
}

function PetnameShowInner({ petname, accounts, otherAccounts, addAccount }) {
    const { id, name } = petname;
    const token = buildPetnameCap({ ...petname, accounts });
    const url = `${HOST}${BASE}/petname/show/#!${token}`;
    const add = React.useCallback(({ account, perms }) => {
        console.log({ foo: "adding", account });
         addAccount({ id, account, rights: perms });
    }, [addAccount, id]);
    return (
        <div className="d-flex flex-column align-items-center">
            <div className="my-5">{name}</div>
            <div className="my-5">
                <ReactQRCode data={url} />
            </div>
            <input
                className="form-control"
                readOnly
                value={url}
                size={url.length}
            />
           <AccountList accounts={accounts} />
           <InitialisedAddAccountForm
               accounts={otherAccounts}
               add={add}
           />
        </div>
    );
}

function mapStateToProps(state, { petnameId }) {
    const petname = (state.keys || {})[petnameId]
    let accounts = [];
    let selectedAccounts = [];
    if (petname && petname.accounts) {
        accounts = petname.accounts.map(({ account, perms }) => {
            const { transactions, ...rest } = state.account[account];
            return { perms, ...rest };
        });
        selectedAccounts =
            petname.accounts.map(({ account }) => account);
    }

    const otherAccounts = Object.values(state.account).filter(
        ({ id }) => !selectedAccounts.includes(id)
    ).map(({ id, name, amount}) => ({
        label: `${name} $(${(amount / 100).toFixed(2)})`,
        value: id,
    }));

    return {
        petname,
        accounts,
        otherAccounts,
    };
}

const ConnectedPetnameShow = connect(
    mapStateToProps,
    { addAccount: petnameAddAccount }
)(PetnameShowInner);

export default function PetnameShow() {
    const { petnameId } = useParams();
    return (
        <div className="container">
            <Help />
            <ConnectedPetnameShow petnameId={petnameId} />
        </div>
    );
}
