import React from 'react';
import { connect } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { createPetname } from '../actions';
import Help from '../help';


function PetnameList({ keys, mkPetname }) {
    const history = useHistory();
    const onNew = React.useCallback(async () => {
        const id = await mkPetname({ name: "New Key", rights: "A" });
        history.push(`/petnames/${id}`);
    }, [history, mkPetname]);

    return (
        <div className="container">
            <Help />
            <p className="lede">List Integrations</p>
            <div>
                <button className="btn btn-success" onClick={onNew}>
                    New
                </button>
            </div>
            {keys.map(({ name, id }) => (
                <div key={id}>
                    <Link to={`/petnames/${id}`}>{name}</Link>
                </div>
            ))}
        </div>
    );
}

function mapStateToProps(state) {
    const keys = Object.values(
        state.keys || {},
        ({ name, id }) => ({ name, id })
    );
    return { keys };
}

export default connect(
    mapStateToProps,
    { mkPetname: createPetname }
)(PetnameList);
