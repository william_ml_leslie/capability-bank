import React from 'react';
import { useRouteMatch, Switch, Route } from 'react-router-dom';
import PetnameShow from './petname-show';
import PetnameList from './petname-list';


export default function PetnameRoutes() {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/:petnameId`}>
                <PetnameShow />
            </Route>
            <Route>
                <PetnameList />
            </Route>
        </Switch>
    );
}
