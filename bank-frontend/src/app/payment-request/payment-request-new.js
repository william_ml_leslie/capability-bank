import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { useHistory } from 'react-router-dom';
import { Input, Select } from '../components/form';
import { createPaymentRequest } from '../actions';
import { BASIS_OPTIONS, SCALE_OPTIONS } from '../model';
import Help from '../help';


const FORM_NAME = "payment-request-new"
const formSelector = formValueSelector(FORM_NAME);


function validateAmount(value, allValues) {
    if (["BILL", "ONGOING"].includes(allValues.basis)) {
        if (Number.parseFloat(value) < 0) {
            return "Must be a non-negative number.";
        }
    } else {
        if (value === "" || Number.parseFloat(value) <= 0) {
            return "Must be a positive number.";
        }
    }
}


function NewRequestForm({ handleSubmit, basis, once }) {
    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <div className="col">
                    <Input
                        name="amount"
                        label="Amount"
                        help="The amount of the invoice."
                        inputType="number"
                        prepend="$"
                        validate={[validateAmount]}
                    />
                </div>
                <div className="col">
                    <Select
                        name="basis"
                        label="Transaction Type"
                        help="The type of the transaction."
                        options={BASIS_OPTIONS}
                    />
                </div>
                <div className="col">
                    <Input
                        name="limitAmount"
                        label="Limit Amount"
                        help="The maximum amount chargeable."
                        inputType="number"
                        prepend="$"
                        disabled={once}
                    />
                </div>
                <div className="col">
                    <Select
                        name="limitScale"
                        label="Limit Scale"
                        help="The time period over which the limit applies."
                        options={SCALE_OPTIONS}
                        disabled={once}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </div>
        </form>
    );
}


const ConnectedRequestForm = reduxForm({ form: FORM_NAME })(NewRequestForm);


function NewRequest({ createPaymentRequest: create, basis = "ONCE" }) {
    const once = ["ONCE", "BOND"].includes(basis);
    const initial = {
        basis: "ONCE",
        amount: once ? 200 : 0,
    };
    const history = useHistory();
    const onSubmit = React.useCallback(
        ({ amount, limitAmount, ...data }) => create({
            amount: amount * 100,
            limitAmount: limitAmount * 100,
            ...data
        }).then(id => history.push(`/payments/${id}`)),
        [create, history]
    );
    return (
        <div className="container">
            <Help />
            <ConnectedRequestForm
                enableReinitialize
                keepDirtyOnReinitialize
                initialValues={initial}
                onSubmit={onSubmit}
                once={once}
            />
        </div>
    );
}


function mapStateToProps(state) {
    return {
        basis: formSelector(state, 'basis'),
    };
}


export default connect(mapStateToProps, { createPaymentRequest })(NewRequest);
