import React from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import ReactQRCode from '../components/react-qrcode';
import { buildPaymentRequestCap } from '../capability';
import Help from '../help';
import RequestShowDetail from './detail-show-request';


const HOST = "http://localhost:3000";
const BASE = process.env.REACT_APP_BASENAME || "";

function RequestShowInner({ request }) {
    const token = buildPaymentRequestCap(request);
    const url = `${HOST}${BASE}/payments/show#!${token}`;
    return (
        <div className="d-flex flex-column align-items-center">
            <RequestShowDetail request={request} />
            <div className="my-5">
                <ReactQRCode data={url} />
            </div>
            <input
                className="form-control"
                readOnly
                value={url}
                size={url.length}
            />
        </div>
    );
}

function mapStateToProps(state, { paymentId }) {
    const selectedRequest = (state.paymentRequest || {})[paymentId];
    return {
        request: selectedRequest,
    };
}

const ConnectedRequestShow = connect(mapStateToProps)(RequestShowInner);

export default function RequestShow() {
    const { paymentId } = useParams();
    return (
        <div className="container">
            <Help />
            <ConnectedRequestShow paymentId={paymentId} />
        </div>
    );
}
