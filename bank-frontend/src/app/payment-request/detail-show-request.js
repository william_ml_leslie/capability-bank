import React from 'react';
import { SCALE } from '../model';

function RequestShowOnce({ amount, url }) {
    return (
        <div>
            A one-off payment of ${(amount / 100).toFixed(2)}.
        </div>
    );
}

function RequestShowBill({ limitAmount, limitScale }) {
    return (
        <div>
            A {SCALE[limitScale]} bill of less than
            ${(limitAmount / 100).toFixed(2)}
        </div>
    );
}

function RequestShowBond({ amount }) {
    return (
        <div>
            A bond of ${(amount / 100).toFixed(2)} that will be repaid
            on termination of the contract.
        </div>
    );
}

function RequestShowOngoing({ amount, limitAmount, limitScale }) {
    return (
        <div>
            A {SCALE[limitScale]} payment of
            ${(limitAmount / 100).toFixed(2)}.
            {amount <= 0 ? null : (
                <>
                    <br />
                    An initial charge of ${(amount / 100).toFixed(2)} will
                    be made to your account.
                </>
            )}
        </div>
    );
}

export default function RequestShowDetail({ request }) {
   const { amount, basis, limitAmount, limitScale } = request;
    if (basis === "ONCE") {
        return (<RequestShowOnce amount={amount} />);
    } else if (basis === "BILL") {
        return (
            <RequestShowBill
                limitAmount={limitAmount}
                limitScale={limitScale}
            />
        );
    } else if (basis === "BOND") {
        return (<RequestShowBond amount={amount} />);
    }
    return (<RequestShowOngoing {...request} />);
}
