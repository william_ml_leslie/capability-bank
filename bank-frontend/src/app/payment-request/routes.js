import React from 'react';
import { useRouteMatch, Switch, Route } from 'react-router-dom';
import PaymentRequestNew from './payment-request-new';
import MerchantShowRequest from './merchant-show-request';
import PatronShowRequest from './patron-show-request';
import UnspecifiedShowRequest from './unspecified-show-request';


export default function TransactionRoutes() {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/new`}>
                <p className="lede">New Transaction Request</p>
                <PaymentRequestNew />
            </Route>
            <Route path={`${path}/list`}>
                List Transacton Requests
            </Route>
            <Route path={`${path}/pay`}>
                <PatronShowRequest />
            </Route>
            <Route path={`${path}/show`}>
                <UnspecifiedShowRequest />
            </Route>
            <Route path={`${path}/:paymentId`}>
                <MerchantShowRequest />
            </Route>
            <Route>
                <p className="lede">New Transaction Request</p>
                <PaymentRequestNew />
            </Route>
        </Switch>
    );
}
