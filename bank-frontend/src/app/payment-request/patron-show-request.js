import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { Input } from '../components/form';
import { decode } from '../capability';
import Help from '../help';
import RequestShowDetail from './detail-show-request.js'


const FORM_NAME = "accept-payment-request";
const formSelector = formValueSelector(FORM_NAME);

const HOST = "http://localhost:3000/";
const BASE = process.env.REACT_APP_BASENAME || "";

function PaymentRequestForm({ handleSubmit }) {
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Input
                    name="paymentRequest"
                    label="Invoice URL"
                    help="The URL or QR Code on the invoice"
                />
            </div>
        </form>
    );
}

const ConnectedRequestForm =
      reduxForm({ form: FORM_NAME })(PaymentRequestForm);

const URL_PATTERN = "^https?y?://([^/]+)([^?#]*)(?:#(.*))$";

// http://localhost:3000//payment-request/show/#!eyJpZCI6MCwiYW1vdW50IjoyMDAwMCwiYmFzaXMiOiJPTkNFIn0=

function parseInvoiceUrl(url) {
    try {
        const re = new RegExp(URL_PATTERN);
        const match = re.exec(url);
        if (match) {
            const token = match[3];
            if (token.startsWith("!")) {
                return decode(token.slice(1));
            }
        }
    } catch (err) {
        // XXX: ignore, but probably want to provide some validation
        // if url is not empty.
        console.log(err);
    }
    return null;
}

function PatronShowRequest({ url }) {
    let request = parseInvoiceUrl(url);
    return (
        <div className="container">
            <Help />
            <ConnectedRequestForm onSubmit={data => console.log(data)} />
            {request && <RequestShowDetail request={request} />}
        </div>
    );
}

function mapStateToProps(state) {
    return {
        url: formSelector(state, 'paymentRequest'),
    };
}

export default connect(mapStateToProps)(PatronShowRequest);

