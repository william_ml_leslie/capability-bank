import React from 'react';
import { useLocation } from 'react-router-dom';
import { decode } from '../capability';
import Help from '../help';
import RequestShowDetail from './detail-show-request';

export default function UnspecifiedShowRequest() {
    let { hash } = useLocation();
    let request = decode(hash.slice(2));
    return (
        <>
            <Help />
            <RequestShowDetail request={request} />
        </>
    );
}
