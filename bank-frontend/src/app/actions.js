// Action creators

export const CREATE_USER = "CREATE_USER";
export const LOGIN_USER = "LOGIN_USER";
export const CREATE_ACCOUNT = "CREATE_ACCOUNT";
export const CREATE_PETNAME = "CREATE_PETNAME";
export const DELETE_PETNAME = "CREATE_PETNAME";
export const ALTER_PETNAME = "CREATE_PETNAME";
export const PETNAME_ADD_ACCOUNT = "PETNAME_ADD_ACCOUNT";
export const PETNAME_DELETE_ACCOUNT = "PETNAME_DELETE_ACCOUNT";
export const PETNAME_EDIT_ACCOUNT = "PETNAME_EDIT_ACCOUNT";
export const CREATE_PAYMENT_REQUEST = "CREATE_PAYMENT_REQUEST";
export const ACCEPT_PAYMENT_REQUEST = "ACCEPT_PAYMENT_REQUEST";
export const CREATE_TRANSACTION = "CREATE_TRANSACTION";

var userId = 0;
var accountId = 0;
var paymentRequestId = 0;
var petnameId = 0;


export const createUserAction = ({ name, amount }) => ({
    type: CREATE_USER, name, amount, id: userId++
});

export const loginUser = ({ name, accounts, keys }) => {
    const allAccounts = {};
    accounts.forEach(account => {
        allAccounts[account.id] = account;
    });
    return {
        type: LOGIN_USER, name, accounts, keys, id: userId++,
    };
};

export const createAccountAction = ({ name, accountType }) => ({
    type: CREATE_ACCOUNT, name, accountType, id: accountId++
});

export const createPetnameAction = ({ account = null, name, rights }) => ({
    type: CREATE_PETNAME, id: petnameId++, name, account, rights
});

export const deletePetname = ({ id }) => ({
    type: DELETE_PETNAME, id
});

export const alterPetname = ({ id, name }) => ({
    type: ALTER_PETNAME, name
});

export const petnameAddAccount = ({ id, account, perms }) => ({
    type: PETNAME_ADD_ACCOUNT, id, account, perms
});

export const petnameDeleteAccount = ({ id, account }) => ({
    type: PETNAME_DELETE_ACCOUNT, id, account
});

export const petnameEditAccount = ({ id, account, perms }) => ({
    type: PETNAME_EDIT_ACCOUNT, id, account, perms
});

export const createPaymentRequestAction = ({
    name, amount, basis, limitAmount, limitScale
}) => ({
    type: CREATE_PAYMENT_REQUEST,
    name,
    amount,
    basis,
    limitAmount,
    limitScale,
    id: paymentRequestId++
});

export const acceptPaymentRequest = ({ name, request }) => ({
    type: ACCEPT_PAYMENT_REQUEST, name, request
});

export const createUser = (data) => async (dispatch) => {
    const action = createUserAction(data);
    await dispatch(action);
    return action.id;
};

export const createPaymentRequest = (data) => async (dispatch) => {
    const action = createPaymentRequestAction(data);
    await dispatch(action);
    return action.id;
};

export const createAccount = (data) => async (dispatch) => {
    const action = createAccountAction(data);
    await dispatch(action);
    return action.id;
};

export const createTransaction = ({
    accountId, amount, label, to
}) => async (dispatch) => ({
    type: CREATE_TRANSACTION, accountId, amount, label, to
});

export const createPetname = (data) => async (dispatch) => {
    const action = createPetnameAction(data);
    await dispatch(action);
    return action.id;
}
