import React from 'react';
import { Field } from 'redux-form';


function inputWithError({
    input,
    name,
    label,
    prepend,
    type,
    help,
    meta: { touched, error }
}) {
    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <div className="input-group">
                {prepend && (
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            {prepend}
                        </div>
                    </div>
                )}
                <input className="form-control" {...input} type={type} />
            </div>
            <small>{help}</small>
            {touched && error && (
                <div className="text-danger">
                    <small>{error}</small>
                </div>
            )}
        </div>
    );
}

export function Input({
    name, label, help, inputType="text", prepend=null, ...rest
}) {
    return (
        <Field
            name={name}
            label={label}
            help={help}
            prepend={prepend}
            type={inputType}
            component={inputWithError}
            {...rest}
        />
    );
}


export function Select({ name, label, help, options, ...rest }) {
    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <Field
                className="form-control"
                name={name}
                type="select"
                component="select"
                {...rest}
            >
                {options.map(({ value, label }) => (
                    <option key={value} value={value}>{label}</option>
                ))}
            </Field>
            <small>{help}</small>
        </div>
    );
}
