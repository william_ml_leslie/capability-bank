import React from 'react';
import QRCode from 'qrcode';

export default function ReactQRCode({ data, ...rest }) {
    const canvasRef = React.useRef(null);
    React.useEffect(() => {
        QRCode.toCanvas(
            canvasRef.current,
            data,
            ({ error }) => error && console.log({ error })
        );
    }, [data])
    return (
        <canvas ref={canvasRef} {...rest} />
    );
}
