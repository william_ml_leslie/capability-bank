import 'babel-polyfill';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, NavLink } from 'react-router-dom';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle';
import AppRoutes from './app/routes';
import { store } from './store';
import './App.css';

function Nav() {
    return (
        <nav className="navbar-nav navbar-expand-md navbar-light bg-info">
            <span className="navbar-brand btn">
                Capability Bank
            </span>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div id="navbarNav" className="collapse navbar-collapse">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/user">User</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/accounts">Accounts</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/payments">Payment Requests</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/payments/pay">Pay An Invoice</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/petnames">Shared Keys</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

function App() {
  return (
    <Provider store={store}>
        <BrowserRouter basename={process.env.REACT_APP_BASENAME || ""}>
        <Nav />
        <div className="App">
          <header className="App-header">
            <AppRoutes />
          </header>
        </div>
        </BrowserRouter>
    </Provider>
  );
}

export default App;
